const fact_lib = require('js-fact-lib');

function fact(n) {
    return fact_lib(n);
}

module.exports = fact;
