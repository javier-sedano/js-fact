#!/usr/bin/env node

const fact = require('..')

process.argv.slice(2).forEach(
    (item) => {
        console.log(fact(item));
    }
);
