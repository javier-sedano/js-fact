const fact = require("../src/index.js");

describe("fact()", () => {
    test("Should return 1 for non-numbers", () => {
        expect(fact("a")).toEqual(1);
    });
});

describe("fact()", () => {
    test("Should return 1 for negative numbers", () => {
        expect(fact(-1)).toEqual(1);
    });
});

describe("fact()", () => {
    test("Should calculate the factorial", () => {
        expect(fact(3)).toEqual(6);
    });
});
